# Gitlab Continuous Integration for a static Site

The purpose of this .gitlab-ci.yml file is to show how to use GitLab to do Continuous Integration with a static site. This file can also be used to for 
Core PHP projects where deployment is done by copy-pasting individual php files manually. I have used Alpine image for the smaller footprint that it uses.

## Project Pre-Requisite
* Install Gitlab Community Edition.
* Install Gitlab Runner.
* Register a Docker Runner. All stages of the CI pipeline will run within the docker image. This will avoid installing multiple dependencies required by individual projects in the Gitlab server.
* Existing repo for .gitlab-ci.yml to use.
* Create a RSA key pair in the Destination Server (Apache/Nginx).
* Copy the Public key (id_rsa.pub) to authorized_keys.
* Private Key will be used by the Gitlab server from Environment Variables.
* rsync to be installed in the Destination Server (Apache/Nginx).
* Host entry for the sample Staging and Production URL to work.

## Environment Variables used in Gitlab
* $PRIVATE_KEY = Private Key of the server where deployment needs to be done. Starting with -----BEGIN PRIVATE KEY-----
* $STAGING_SRC_DIR = /builds/root/jquerymobile-test/
* $STAGING_DEST_DIR = /usr/share/nginx/html/staging/jquerymobile-test
* $PRODUCTION_SRC_DIR = /builds/root/jquerymobile-test/
* $PRODUCTION_DEST_DIR = /usr/share/nginx/html/production/jquerymobile-test
* $USER_NAME = root
* $HOSTNAME = IP Address of the server where deployment needs to be done.

## Nginx Configuration
I have used the same Nginx server to create 2 hostnames to test automatic deployment in Staging followed by manual deployment in Production.

```nginx
server {
    listen       80;
    server_name  staging-localhost;

    location / {
        root   /usr/share/nginx/html/staging/gitlab-ci/src;
        index  index.html index.htm;
    }
}
```

[Staging URL - http://staging-localhost/](http://staging-localhost/)

```nginx
server {
    listen       80;
    server_name  production-localhost;

    location / {
        root   /usr/share/nginx/html/production/gitlab-ci/src;
        index  index.html index.htm;
    }
}
```

[Production URL - http://production-localhost/](http://production-localhost/)

## Gitlab CI Pipeline
![Gitlab ci pipeline](https://gitlab.com/pratyush.majumdar/gitlab-ci/raw/master/images/pipeline.png)

## Gitlab Workflow
![Gitlab ci pipeline flow](https://gitlab.com/pratyush.majumdar/gitlab-ci/raw/master/images/pipeline-new.png)

## App Homepage
![App Home Page](https://gitlab.com/pratyush.majumdar/gitlab-ci/raw/master/images/app-home.png)

